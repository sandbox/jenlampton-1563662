<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <title><?php print $head_title ?></title>
  </head>
  <body <?php print drupal_attributes($attr) ?>>

  <?php print $skipnav ?>

  <div id='branding' class='clear-block'>
    <div class='breadcrumb clear-block'><?php print $breadcrumb ?></div>
    <a href="/" class="home-link"><span>Home</span></a>

	<div id='nav_primary'>
		<?php print $nav_primary ?>
	</div>

  </div>

  <div id='page-title' class='clear-block'>
    <?php if ($tabs): ?><?php print $tabs ?><?php endif; ?>
    <h1 class='page-title <?php print $page_icon_class ?>'>
      <?php if (!empty($page_icon_class)): ?><span class='icon'></span><?php endif; ?>
      <?php if ($title) print $title ?>
    </h1>
  </div>