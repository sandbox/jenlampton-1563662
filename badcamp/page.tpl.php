<?php include('header.inc'); ?>

  <div id='page'>
    <?php if ($tabs2): ?><div class='secondary-tabs clear-block'><?php print $tabs2 ?></div><?php endif; ?>
    <?php if ($help) print $help ?>
    <div class='page-content clear-block'>
      <?php if ($show_messages && $messages): ?>
        <div id='console' class='clear-block'><?php print $messages; ?></div>
      <?php endif; ?>

      <div id='content'>
        <?php if (!empty($content)): ?>
          <div class='content-wrapper clear-block'><?php print $content ?></div>
        <?php endif; ?>
        <?php print $content_region ?>
      </div>
    </div>
  </div>

  <?php include('footer.inc'); ?>

